﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicDecalProjector : MonoBehaviour {

	private const string DECAL_NAME = "DynamicDecal";
	private const string DECAL_MESH_NAME = "DynamicDecalMesh";
	public const int DECAL_LAYER = 8;

	[SerializeField]
	private Material decalMaterial;
	[SerializeField]
	private float width = 0.5f;
	[SerializeField]
	private float height = 0.5f;
	[SerializeField]
	private float depth = 0.5f;
	[SerializeField]
	private float facingFactor = 0.5f;

	private List<Vector3> decalVertices;
	private List<Color> decalColors;
	private List<int> decalTriangles;
	private List<Vector2> decalUVs;

	private Vector3 localNrm;
	private Vector3 localTan;
	private Vector3 localBitan;

	private Plane frontPlane;
	private Plane backPlane;
	private Plane rightPlane;
	private Plane leftPlane;
	private Plane topPlane;
	private Plane bottomPlane;

	private float triangleFacingFactor;

	private AbstractPlaneTriangleResolver[] resolvers;
	private IndexedVertex[] orderedIndexedVerticesBuffer;
	private int[] triangleBuilderBuffer;
	private bool[] splitTriangleEdges;

	private void Awake()
	{
		triangleBuilderBuffer = new int[3];
		orderedIndexedVerticesBuffer = new IndexedVertex[6];
		splitTriangleEdges = new bool[3]{false, false, false};

		resolvers = new AbstractPlaneTriangleResolver[8];
		ClippingPlaneTriangleResolver clippingPlaneTriangleResolver = new ClippingPlaneTriangleResolver(this);

		resolvers[0] = new DiscardPlaneTriangleResolver(this);
		resolvers[1] = clippingPlaneTriangleResolver;
		resolvers[2] = clippingPlaneTriangleResolver;
		resolvers[3] = clippingPlaneTriangleResolver;
		resolvers[4] = clippingPlaneTriangleResolver;
		resolvers[5] = clippingPlaneTriangleResolver;
		resolvers[6] = clippingPlaneTriangleResolver;
		resolvers[7] = new PassPlaneTriangleResolver(this);
	}

	public GameObject GenerateDynamicDecal(Vector3 pos, Vector3 nrm, Vector3 tan)
	{
		GameObject decalGameObject = new GameObject (DECAL_NAME);
		decalGameObject.transform.localScale = Vector3.one;
		decalGameObject.transform.position = pos;
		MeshFilter meshFilter = decalGameObject.AddComponent<MeshFilter> ();
		MeshRenderer meshRenderer = decalGameObject.AddComponent<MeshRenderer> ();
		meshRenderer.materials = new Material[1]{ decalMaterial };
		meshRenderer.motionVectorGenerationMode = MotionVectorGenerationMode.ForceNoMotion;
		meshRenderer.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
		meshRenderer.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;
		meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		meshRenderer.receiveShadows = false;

		float halfWidth = width * 0.5f;
		float halfHeight = height * 0.5f;
		float halfDepth = depth * 0.5f;

		float radius = Mathf.Sqrt(halfWidth * halfWidth + halfHeight * halfHeight + halfDepth * halfDepth);

		decalGameObject.transform.rotation = Quaternion.LookRotation (nrm, tan);
		localNrm = Vector3.forward;
		localTan = Vector3.up;
		localBitan = Vector3.right;
	
		frontPlane = new Plane (-localNrm, halfDepth);
		backPlane = new Plane (localNrm, halfDepth);
		rightPlane = new Plane (-localBitan, halfWidth);
		leftPlane = new Plane (localBitan, halfWidth);
		topPlane = new Plane (-localTan, halfHeight);
		bottomPlane = new Plane (localTan, halfHeight);

		Mesh mesh = new Mesh ();
		mesh.name = DECAL_MESH_NAME;
		Collider[] colliders = Physics.OverlapSphere (pos, radius, 1 << DECAL_LAYER, QueryTriggerInteraction.Ignore);
		Matrix4x4 world2LocalDecalMatrix = decalGameObject.transform.worldToLocalMatrix;

		decalColors = new List<Color>();
		decalVertices = new List<Vector3> ();
		decalTriangles = new List<int> ();
		decalUVs = new List<Vector2>();

		LinkedList<SimpleTriangle> clippedTriangleBuffer = new LinkedList<SimpleTriangle> ();

		int collidersLength = colliders.Length;

		for (int i = 0; i < collidersLength; i++) {
			
			MeshCollider meshCollider = colliders[i] as MeshCollider;
			Mesh colliderMesh = meshCollider.sharedMesh;
			Matrix4x4 local2WorldMeshColliderMatrix = meshCollider.transform.localToWorldMatrix;
			Matrix4x4 localMeshCollider2LocalDecal =  world2LocalDecalMatrix * local2WorldMeshColliderMatrix;
			Matrix4x4 invTransLocalMeshCollider2LocalDecal = localMeshCollider2LocalDecal.inverse.transpose;
			int[] meshTriangles = colliderMesh.triangles;
			Vector3[] meshVertices = colliderMesh.vertices;
			int[] indexMap = new int[meshVertices.Length];
			Vector3[] meshNormals = colliderMesh.normals;
			int meshTrianglesLength = meshTriangles.Length;
			int triangleCounter = 0;

			while (triangleCounter < meshTrianglesLength) {

				int v1OldIndex = meshTriangles [triangleCounter];
				int v2OldIndex = meshTriangles [triangleCounter + 1];
				int v3OldIndex = meshTriangles [triangleCounter + 2];

				triangleCounter += 3;

				IndexedVertex v1 = GetIndexedVertex (v1OldIndex, localMeshCollider2LocalDecal, meshVertices, indexMap);
				IndexedVertex v2 = GetIndexedVertex (v2OldIndex, localMeshCollider2LocalDecal, meshVertices, indexMap);
				IndexedVertex v3 = GetIndexedVertex (v3OldIndex, localMeshCollider2LocalDecal, meshVertices, indexMap);

				Vector3 midVertNrm = meshNormals [v1OldIndex] + meshNormals [v2OldIndex] + meshNormals [v3OldIndex];
				midVertNrm = invTransLocalMeshCollider2LocalDecal.MultiplyVector (midVertNrm).normalized;

				Vector3 v1Vertex = v1.vertex;
				Vector3 triangleNrm = Vector3.Cross (v2.vertex - v1Vertex, v3.vertex - v1Vertex).normalized;

				if (Vector3.Dot (triangleNrm, midVertNrm) < 0) {
					triangleNrm = -triangleNrm;
				}

				triangleFacingFactor = Vector3.Dot(triangleNrm, localNrm);
				if (triangleFacingFactor < facingFactor)
				{
					continue;
				}

				SimpleTriangle inputTriangle = new SimpleTriangle (v1, v2, v3);
				clippedTriangleBuffer.Clear ();
				clippedTriangleBuffer.AddLast (inputTriangle);

				ClipTrianglesByPlane (clippedTriangleBuffer, frontPlane);
				ClipTrianglesByPlane (clippedTriangleBuffer, backPlane);
				ClipTrianglesByPlane (clippedTriangleBuffer, rightPlane);
				ClipTrianglesByPlane (clippedTriangleBuffer, leftPlane);
				ClipTrianglesByPlane (clippedTriangleBuffer, topPlane);
				ClipTrianglesByPlane (clippedTriangleBuffer, bottomPlane);

				LinkedList<SimpleTriangle>.Enumerator triangleEnumerator = clippedTriangleBuffer.GetEnumerator();
				while (triangleEnumerator.MoveNext ()) {
					AddTriangle (triangleEnumerator.Current, indexMap);
				}
			}
		}

		
		mesh.vertices = decalVertices.ToArray ();
		mesh.triangles = decalTriangles.ToArray ();
		mesh.SetUVs(0, decalUVs);
		mesh.colors = decalColors.ToArray();
		mesh.RecalculateNormals ();
		mesh.RecalculateBounds ();
		meshFilter.mesh = mesh;

		return decalGameObject;
	}

	private void ClipTrianglesByPlane(LinkedList<SimpleTriangle> buffer, Plane plane)
	{
		LinkedListNode<SimpleTriangle> currentNode;
		LinkedListNode<SimpleTriangle> nextNode = buffer.First;

		while (nextNode != null) {

			currentNode = nextNode;
			nextNode = nextNode.Next;

			SimpleTriangle triangle = currentNode.Value;

			Vector3 v1 = triangle.v1.vertex;
			Vector3 v2 = triangle.v2.vertex;
			Vector3 v3 = triangle.v3.vertex;

			int resolverIndex = 0;

			resolverIndex |= plane.GetSide(v1) ? 1 : 0;
			resolverIndex |= plane.GetSide(v2) ? 1 << 1 : 0;
			resolverIndex |= plane.GetSide(v3) ? 1 << 2 : 0;

			resolvers[resolverIndex].Resolve(plane, resolverIndex, currentNode, buffer);
		}
	}

	private IndexedVertex GetIndexedVertex(int oldIndex, Matrix4x4 m, Vector3[] meshVertices, int[] indexes)
	{
		int newIndex = indexes [oldIndex];
		return (newIndex > 0) 
			? new IndexedVertex (decalVertices [newIndex - 1], oldIndex, newIndex) 
				: new IndexedVertex (m.MultiplyPoint3x4 (meshVertices [oldIndex]), oldIndex, newIndex);
	}

	private void AddTriangle(SimpleTriangle t, int[] indexMap)
	{
		AddTriangleVertex (t.v1, indexMap);
		AddTriangleVertex (t.v2, indexMap);
		AddTriangleVertex (t.v3, indexMap);
	}

	private void AddTriangleVertex(IndexedVertex v, int[] indexMap)
	{
		int newIndex = v.newIndex;
		int oldIndex = v.oldIndex;

		if (oldIndex < 0) {
			//generated vertices
			decalTriangles.Add (newIndex);
			return;
		}

		//shared original vertices
		if (newIndex > 0) {
			decalTriangles.Add (newIndex - 1);
			return;
		}
			
		//firstly added original vertex
		newIndex = decalVertices.Count;
		indexMap[oldIndex] = newIndex + 1;
		AddVertexToDecalVertices(v.vertex);
		decalTriangles.Add (newIndex);
	}

	private void AddVertexToDecalVertices(Vector3 v)
	{
		float s = leftPlane.GetDistanceToPoint(v) / width;
		float t = bottomPlane.GetDistanceToPoint(v) / height ;

		float r = (triangleFacingFactor - facingFactor) / (1 - facingFactor);
		float g = Mathf.Abs((2 * new Plane(localNrm, Vector3.zero).GetDistanceToPoint(v)) / depth);
		
		decalColors.Add(new Color(r,g,0));
		decalVertices.Add(v);
		decalUVs.Add(new Vector2(s, t));
	}

	private void WriteVerticesToOrderedBuffer(SimpleTriangle t)
	{
		IndexedVertex[] buffer = orderedIndexedVerticesBuffer;
		buffer[0] = t.v1;
		buffer[2] = t.v2;
		buffer[4] = t.v3;
	}

	private struct IndexedVertex
	{
		public readonly Vector3 vertex;
		public readonly int oldIndex;
		public readonly int newIndex;

		public IndexedVertex (Vector3 vertex, int oldIndex, int newIndex)
		{
			this.vertex = vertex;
			this.oldIndex = oldIndex;
			this.newIndex = newIndex;
		}
	}

	private struct SimpleTriangle
	{
		public readonly IndexedVertex v1;
		public readonly IndexedVertex v2;
		public readonly IndexedVertex v3;

		public SimpleTriangle (IndexedVertex v1, IndexedVertex v2, IndexedVertex v3)
		{
			this.v1 = v1;
			this.v2 = v2;
			this.v3 = v3;
		}
	}

	private abstract class AbstractPlaneTriangleResolver
	{
		protected DynamicDecalProjector projector;
		protected AbstractPlaneTriangleResolver(DynamicDecalProjector projector)
		{
			this.projector = projector;
		}

		public abstract void Resolve(Plane plane, int mask, LinkedListNode<SimpleTriangle> inputNode, LinkedList<SimpleTriangle> buffer);
	}

	private class PassPlaneTriangleResolver : AbstractPlaneTriangleResolver
	{
		public PassPlaneTriangleResolver (DynamicDecalProjector projector) : base (projector)
		{
		}
		

		public override void Resolve(Plane plane, int mask, LinkedListNode<SimpleTriangle> inputNode, LinkedList<SimpleTriangle> buffer)
		{
		}
	}

	private class DiscardPlaneTriangleResolver : AbstractPlaneTriangleResolver
	{
		public DiscardPlaneTriangleResolver (DynamicDecalProjector projector) : base (projector)
		{
		}
		

		public override void Resolve(Plane plane, int mask, LinkedListNode<SimpleTriangle> inputNode, LinkedList<SimpleTriangle> buffer)
		{
			buffer.Remove(inputNode);
		}
	}

	private class ClippingPlaneTriangleResolver : AbstractPlaneTriangleResolver
	{
		public ClippingPlaneTriangleResolver (DynamicDecalProjector projector) : base (projector)
		{
		}

		private void SplitTriangleEdge(Plane plane, int edgeIndex)
		{
			bool[] splitTriangleEdges = projector.splitTriangleEdges;
			if (!splitTriangleEdges[edgeIndex])
			{
				//no intersection with plane
				return;
			}

			int vertIndex = edgeIndex * 2 + 1;
			int fromVertexIndex = vertIndex - 1;
			int toVertexIndex = vertIndex + 1;
			toVertexIndex = toVertexIndex == 6 ? 0 : toVertexIndex;

			IndexedVertex[] orderedIndexedVerticesBuffer = projector.orderedIndexedVerticesBuffer;
			Vector3 fromVert = orderedIndexedVerticesBuffer[fromVertexIndex].vertex;
			Vector3 toVert = orderedIndexedVerticesBuffer[toVertexIndex].vertex;
			Vector3 edgeVec = toVert - fromVert;
			float edgeLength = edgeVec.magnitude;
			Vector3 edgeDir = Vector3.Normalize(edgeVec);

			Ray ray = new Ray(fromVert, edgeDir);
			float distance;
			bool hasIntersection = plane.Raycast(ray, out distance);

			splitTriangleEdges[edgeIndex] = hasIntersection && (distance > 0) && (distance < edgeLength);

			if (!splitTriangleEdges[edgeIndex])
			{
				//nearby vertices and plane
				return;
			}

			//early add generated vertex
			List<Vector3> decalVertices = projector.decalVertices;
			int newIndex = decalVertices.Count;
			IndexedVertex generatedVertex = new IndexedVertex(fromVert + edgeDir * distance, -1, newIndex);
			projector.AddVertexToDecalVertices(generatedVertex.vertex);
			orderedIndexedVerticesBuffer[vertIndex] = generatedVertex;
		}

		private LinkedListNode<SimpleTriangle> PushVertexToTriangleBuilderBuffer(
			int orderedBufferIndex, 
			ref int builderIndex,
			ref int firstAcceptedVertexIndex,
			ref int i,
			LinkedListNode<SimpleTriangle> node, 
			LinkedList<SimpleTriangle> buffer
		)
		{
			int[] triangleBuilderBuffer = projector.triangleBuilderBuffer;
			triangleBuilderBuffer[builderIndex] = orderedBufferIndex;
			builderIndex++;
			if (builderIndex == 3)
			{
				Array.Sort(triangleBuilderBuffer);
				IndexedVertex[] orderedIndexedVerticesBuffer = projector.orderedIndexedVerticesBuffer;
				buffer.AddAfter(node, new SimpleTriangle(
					orderedIndexedVerticesBuffer[triangleBuilderBuffer[0]],
					orderedIndexedVerticesBuffer[triangleBuilderBuffer[1]],
					orderedIndexedVerticesBuffer[triangleBuilderBuffer[2]]
				));

				builderIndex = 0;
				i = orderedBufferIndex;
				node = node.Next;
			}
			firstAcceptedVertexIndex = firstAcceptedVertexIndex < 0 ? orderedBufferIndex : firstAcceptedVertexIndex;
			return node;
		}

		public override void Resolve(Plane plane, int mask, LinkedListNode<SimpleTriangle> inputNode, LinkedList<SimpleTriangle> buffer)
		{
			LinkedListNode<SimpleTriangle> currentNode = inputNode;
			SimpleTriangle triangle2Clip = inputNode.Value;
			projector.WriteVerticesToOrderedBuffer(triangle2Clip);
			BitArray vertexSidesMask = new BitArray(new byte[1] { BitConverter.GetBytes(mask)[0] });

			bool v1Side = vertexSidesMask.Get(0);
			bool v2Side = vertexSidesMask.Get(1);
			bool v3Side = vertexSidesMask.Get(2);

			bool[] splitTriangleEdges = projector.splitTriangleEdges;
			splitTriangleEdges[0] = v1Side ^ v2Side;
			splitTriangleEdges[1] = v2Side ^ v3Side;
			splitTriangleEdges[2] = v3Side ^ v1Side;

			SplitTriangleEdge(plane, 0);
			SplitTriangleEdge(plane, 1);
			SplitTriangleEdge(plane, 2);

			int firstAcceptedVertexIndex = -1;
			bool secondTimeFirstVertexProcessing = false;
			int i = 0;
			int currentIndex = 0;
			int triangleBuilderIndex = 0;

			do
			{
				currentIndex = i;
				i++;
				i = (i == 6) ? 0 : i;

				bool isNativeVertex = ((currentIndex % 2) == 0);
				secondTimeFirstVertexProcessing = (firstAcceptedVertexIndex >= 0);

				if (!isNativeVertex)
				{
					if (splitTriangleEdges[currentIndex / 2])
					{
						currentNode = PushVertexToTriangleBuilderBuffer(
							currentIndex,
							ref triangleBuilderIndex,
							ref firstAcceptedVertexIndex,
							ref i,
							currentNode,
							buffer
						);
					}

					continue;
				}
					
				//skip native vertex that is on negative side
				if (!vertexSidesMask.Get(currentIndex / 2))
				{
					continue;
				}

				//push native vertex that is on positive side
				currentNode = PushVertexToTriangleBuilderBuffer(
					currentIndex,
					ref triangleBuilderIndex,
					ref firstAcceptedVertexIndex,
					ref i,
					currentNode,
					buffer
				);

			} while (
					((currentIndex != firstAcceptedVertexIndex) || (!secondTimeFirstVertexProcessing)) && // end of triangulation
					!((i == 0) && !secondTimeFirstVertexProcessing) //one cycle without insert in triangle builder buffer
			);
				
			buffer.Remove(inputNode);
		}
	}
}
