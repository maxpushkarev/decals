﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicDecalController : MonoBehaviour {

	[SerializeField]
	private DynamicDecalProjector decalProjector;

	private GameObject decalGameObject;

	private void Update()
	{
		if (!Input.GetMouseButtonDown (0)) {
			return;
		}

		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

		if (Physics.Raycast (ray, out hit, 1e6f, 1 << DynamicDecalProjector.DECAL_LAYER, QueryTriggerInteraction.Ignore))
		{
			//Vector3 nrm = Vector3.Normalize(hit.normal - ray.direction);
			Vector3 nrm = hit.normal;
			Vector3 tangent = Vector3.Cross(nrm, Camera.main.transform.up);
			if (tangent.sqrMagnitude < 0.5) {
				tangent = Vector3.Cross(nrm, Camera.main.transform.right);
			}
			tangent = Vector3.Normalize (tangent);


			GameObject newDecal = decalProjector.GenerateDynamicDecal (hit.point, nrm, tangent);
			if (decalGameObject) {
				GameObject.Destroy (decalGameObject);
			}
			decalGameObject = newDecal;
		}
	}
}
