﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MeshColliderGenerator : EditorWindow {

	[MenuItem("Decals/Generate Mesh Colliders")]
	public static void Generate()
	{
		Renderer[] renderers = Renderer.FindObjectsOfType<Renderer> ();
		foreach (Renderer renderer in renderers) {
			DynamicDecalCollider[] colliders = renderer.gameObject.GetComponentsInChildren<DynamicDecalCollider> (true);
			foreach (DynamicDecalCollider collider in colliders) {
				GameObject.DestroyImmediate (collider.gameObject);
			}
			GameObject go = new GameObject ("DecalCollider");
			go.AddComponent<DynamicDecalCollider> ();
			go.transform.SetParent (renderer.transform);
			go.transform.localPosition = Vector3.zero;
			go.transform.localRotation = Quaternion.identity;
			go.transform.localScale = Vector3.one;
			go.layer = DynamicDecalProjector.DECAL_LAYER;
			go.isStatic = true;

			MeshCollider meshCollider = go.AddComponent<MeshCollider> ();
			meshCollider.convex = false;
			meshCollider.sharedMesh = renderer.GetComponent<MeshFilter> ().sharedMesh;

		}

		Debug.Log ("mesh colliders generated");
	}

}
