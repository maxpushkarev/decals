﻿Shader "Decal" {

	Properties {
		_MainTex("Texture", 2D) = "white" {}
	}

	SubShader {

		Tags { 
			"Queue"="Overlay"
			"RenderType"="Transparent"
			"LightMode" = "ForwardBase" 
			"IgnoreProjector" = "True"
		}

		Pass
		{	
			Cull Back
			ZWrite On
			ZTest LEqual

			Offset -3, -3
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM

 		 		#pragma target 3.0   
         		#pragma vertex vert  
         		#pragma fragment frag

         		#include "UnityCG.cginc"

				uniform half4 _LightColor0;
				sampler2D _MainTex;
							
				struct vertexInput {
				     half4 vertex : POSITION;
				     float2 uv : TEXCOORD0;
					 float3 color : TEXCOORD1;
				};
								
				struct vertexOutput {
				    half4 pos : SV_POSITION;
				    float2 uv : TEXCOORD0;
					float3 color : TEXCOORD1;
				};
								
								
				vertexOutput vert(vertexInput v) 
				{
				     vertexOutput output;
				     output.pos = mul(UNITY_MATRIX_MVP, v.vertex); 
				     output.uv = v.uv;
					 output.color = v.color;
				     return output;
				}
								
				half4 frag(vertexOutput input) : COLOR
				{		
					float4 texCol = tex2D(_MainTex, input.uv);
					float3 col = input.color;
					texCol.a *= saturate(col.r);
					//texCol.a *= saturate(1.0h - col.g);
					return texCol;
				}


			ENDCG
		}
	}

	Fallback "Diffuse"
}
